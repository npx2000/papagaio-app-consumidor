import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProdutoPage } from './index.page';
import { ProdutoaddPage } from './add.page';

const routes: Routes = [
  { path: '', component: ProdutoPage },
  { path: 'add', component: ProdutoaddPage },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [ProdutoaddPage],
  declarations: [ProdutoPage, ProdutoaddPage]
})
export class ProdutoPageModule {}
