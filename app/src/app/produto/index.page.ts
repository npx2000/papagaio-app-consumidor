import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProdutoaddPage } from './add.page';
import { Produto } from '../produto';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class ProdutoPage implements OnInit {

  produtos: Produto[];

  constructor(public modalController: ModalController, private apiService: ApiService) { }

  ngOnInit() {
    this.getProdutos();
  }

  getProdutos(): void {
    this.apiService.getProdutos().subscribe(produtos => this.produtos = produtos);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ProdutoaddPage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

}
