import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Produto } from './produto';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private url = 'http://api-papagaio-dev-com-br.umbler.net/produto/';  // URL to web api

  constructor(
    private http: HttpClient) { }

  /** GET heroes from the server */
  getProdutos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.url)
      .pipe(
        tap(_ => this.log('fetched produtos')),
        catchError(this.handleError<Produto[]>('getProdutos', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`${message}`);
    // this.messageService.add(`HeroService: ${message}`);
  }
}
